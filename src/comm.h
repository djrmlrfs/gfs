#ifndef GFS_COMMON_H
#define GFS_COMMON_H
#include <map>
#include <set>
#include <mutex>
#include <ctime>
#include <vector>
#include <atomic>
#include <chrono>
#include <string>
#include <thread>
#include <iostream>
#include <msgpack.hpp>
#include <unordered_map>

typedef std::uint64_t ChunkHandle;
typedef std::uint64_t ChunkVersion;

const int CHUNK_SIZE =  67108864;

enum class GFSErrorCode : std::uint32_t{
	OK = 0,csCreate,csOpen,csRead,csWrite,csCpy,csFul,csEx,
	mNosev,mLksev,mNock,mNorp,mNofl,mHvfl,mWrnm,mDr,mFl,mNFl,mWrid};
struct GFSError
{
	GFSErrorCode errCode;
	std::string description;
	GFSError():errCode(GFSErrorCode::OK), description("ok"){}
	GFSError(GFSErrorCode ecd,std::string des):errCode(ecd), description(des){}
	bool hverr(){return (errCode!=GFSErrorCode::OK);}
	MSGPACK_DEFINE(errCode, description);
};

MSGPACK_ADD_ENUM(GFSErrorCode);
const std::chrono::seconds LMax(60), HPAS(3), BPas(5), St(10), GPas(240);

#endif
