#include "chunkserver.h"
template<typename Ret, typename ...Args>
void ChunkServer::bind(std::string rpcName, Ret(ChunkServer::*func)(Args...))
{
	srv.RPCBind(rpcName, std::function<Ret(Args...)>([this, func](Args ...args) -> Ret 
	{
		return (this->*func)(std::forward<Args>(args)...);
	}));
}
ChunkServer::chunkInformation::chunkInformation():isp(0), sern(0), tsmp(0){}
ChunkServer::chunkInformation::chunkInformation(const bool&ip, const ChunkVersion&vs, const std::uint64_t&ts, const std::uint64_t&sn) : isp(ip), ver(vs), tsmp(ts), sern(sn){}
ChunkServer::ChunkServer(LightDS::Service &srv, const std::string &rootDir):srv(srv), rootDir(rootDir), on(0)
{
    bind("RPCPushData", &ChunkServer::RPCPushData);
    bind("RPCSendCopy", &ChunkServer::RPCSendCopy);
    bind("RPCApplyCopy", &ChunkServer::RPCApplyCopy);
    bind("RPCReadChunk", &ChunkServer::RPCReadChunk);
    bind("RPCWriteChunk", &ChunkServer::RPCWriteChunk);
    bind("RPCGrantLease", &ChunkServer::RPCGrantLease);
	bind("RPCCreateChunk", &ChunkServer::RPCCreateChunk);
    bind("RPCAppendChunk", &ChunkServer::RPCAppendChunk);
    bind("RPCUpdateVersion", &ChunkServer::RPCUpdateVersion);
    bind("RPCApplyMutation", &ChunkServer::RPCApplyMutation);
}
void ChunkServer::Start()
{
	on = 1;
	struct dirent *ent = NULL;
	DIR *dir = opendir(rootDir.c_str());
	if (dir == NULL)
	{
		std::thread ThreadHeartBeat(&ChunkServer::Heartbeat,this);
		ThreadHeartBeat.detach();	return;
	}
	while ((ent=readdir(dir)) != NULL)
	{
		if (strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0)	continue;
		std::string Name = ent->d_name;
		if (Name[0]>='0' && Name[0]<='9')
		{
			std::string _handle;
			for (int i = 0; i < Name.size(); ++i)
			{
				if (Name[i] == 'v')
				{
					const char *_fileName = _handle.c_str();
					uint64_t handle =  strtoull(_fileName,NULL,0);
					std::ifstream fin(Name.c_str());
					std::string _version;	fin >> _version;
					uint64_t ver = strtoull(_version.c_str(),NULL,0);
					chunkMap[handle].ver = ver;		break;
				}
				else if (Name[i] == 's')
				{
					const char *_fileName = _handle.c_str();
					uint64_t handle =  strtoull(_fileName,NULL,0);
					std::ifstream fin(Name.c_str());
					std::string _serialNo;	fin >> _serialNo;
					uint64_t sern = strtoull(_serialNo.c_str(),NULL,0);
					chunkMap[handle].sern = sern;	break;
				}
				_handle += Name[i];
			}
		}
	}
	closedir(dir);
	std::thread ThreadHeartBeat(&ChunkServer::Heartbeat,this);
	ThreadHeartBeat.detach();
}
void ChunkServer::Shutdown()
{
	on = 0;
}
void ChunkServer::Heartbeat()
{
	while (on)
	{
		std::vector<ChunkHandle> leaseExtensions, failedChunks;
		std::vector<std::tuple<ChunkHandle,ChunkVersion> > chunks;
		for (auto iter = chunkMap.begin(); iter != chunkMap.end(); ++iter)
		{
			ChunkHandle handle = iter->first;
			std::string Name = std::to_string(handle);
			std::ifstream RF((rootDir+Name).c_str());
			if (!RF)	failedChunks.push_back(handle);
			RF.close();
			if (iter->second.isp == 1)	if (std::chrono::seconds(time(0)-iter->second.tsmp) > LMax)	leaseExtensions.push_back(handle);
			chunks.push_back(std::tuple<ChunkHandle,ChunkVersion>(handle,iter->second.ver));
		}
		LightDS::Service::RPCAddress addr = srv.ListService("master")[0];
		auto result = srv.RPCCall(addr,"RPCHeartbeat",leaseExtensions,chunks,failedChunks).get().as<std::tuple<GFSError,std::vector<ChunkHandle> > >();
		if (!std::get<0>(result).hverr())
		{
			std::vector<ChunkHandle> garbage = std::get<1>(result);
			for (int i = 0; i < garbage.size(); ++i)
			{
				std::string Name = rootDir+std::to_string(garbage[i]);
				std::string N1 = Name+"vs", N2 = Name+"sn", path = Name;
	            std::remove(path.c_str());
	            std::remove(N1.c_str());
	            std::remove(N2.c_str());
	            chunkMap.erase(chunkMap.find(garbage[i]));
			}
		}
		std::this_thread::sleep_for(HPAS);
	}
}
GFSError ChunkServer::RPCCreateChunk(ChunkHandle handle)
{
	std::ofstream CF((rootDir+std::to_string(handle)).c_str());
	if (!CF)	return GFSError(GFSErrorCode::csCreate, "create file fail");
	chunkMap[handle] = chunkInformation();
	CF.close();	return GFSError();
}
std::tuple<GFSError, std::string > ChunkServer::RPCReadChunk(ChunkHandle handle, std::uint64_t offset, std::uint64_t length)
{
	std::ifstream RF((rootDir+std::to_string(handle)).c_str());
	if (!RF)	return std::tuple<GFSError,std::string>(GFSError(GFSErrorCode::csOpen,"open file fail"),"");
	std::string data;	RF.seekg(offset);
	char ch;
	for (int i = 1; i <= length; ++i)
	{
		RF.read(reinterpret_cast<char*>(&ch),sizeof(char));
		if (RF.eof())	break;	data += ch;
	}
	RF.close();
	return std::tuple<GFSError,std::string>(GFSError(),data);
}
GFSError ChunkServer::RPCWriteChunk(ChunkHandle handle, std::uint64_t dataID, std::uint64_t offset, std::vector<std::string> secondaries)
{
	std::string Name = rootDir+std::to_string(handle), N1 = Name+"sn";
	std::fstream WC(Name.c_str(),std::ios::app|std::ios::binary);
	if (!WC)	return GFSError(GFSErrorCode::csOpen,"open file fail");
    if (dataMap.find(dataID) == dataMap.end())	return GFSError(GFSErrorCode::csRead,"no such data id");
	WC.seekg(0,std::ios::end);	while (WC.tellg() < offset){char ch = 0;WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}WC.seekp(offset);
	std::string data = dataMap[dataID];
	for (int i = 0; i < data.size(); ++i)
		{char ch = data[i];WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
	WC.close();
	if (chunkMap.find(handle) == chunkMap.end())	return GFSError(GFSErrorCode::csOpen, "no such chunk");
	std::uint64_t sern = ++chunkMap[handle].sern;
	std::ofstream fout(N1.c_str());
	fout << sern;	fout.close();
	bool err = 0;
	for (int i = 0; i < secondaries.size(); ++i)
		if (srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]),
			"RPCApplyMutation",handle,sern,MutationWrite,dataID,offset,data.size()).get().as<GFSError>().hverr())
				{err = 1; break;}
	if (err)	return GFSError(GFSErrorCode::csWrite, "apply mutation fail");
	else	return GFSError();
}
std::tuple<GFSError, std::uint64_t> ChunkServer::RPCAppendChunk(ChunkHandle handle, std::uint64_t dataID, std::vector<std::string> secondaries)
{
	std::string Name = rootDir+std::to_string(handle), N1 = Name+"sn";
	std::fstream WC(Name.c_str(),std::ios::app|std::ios::binary);
	if (!WC)	return std::tuple<GFSError,std::uint64_t>(GFSError(GFSErrorCode::csOpen,"open file fail"), 0);
	if (dataMap.find(dataID) == dataMap.end())
		return std::tuple<GFSError,std::uint64_t>(GFSError(GFSErrorCode::csRead,"no such data id"),0);
	std::string data = dataMap[dataID];
	WC.seekg(0,std::ios::end);
	int size = WC.tellg();
	std::uint64_t sern = ++chunkMap[handle].sern;
	std::ofstream fout(N1.c_str());
	fout << sern;	fout.close();
	if (size+data.size() > CHUNK_SIZE)
	{
		while (WC.tellg() != CHUNK_SIZE)
			{char ch = 0;WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
		bool err = 0;
		for (int i = 0; i < secondaries.size(); ++i)
			if (srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]),"RPCApplyMutation",handle,sern,MutationPad,dataID,0,data.size()).get().as<GFSError>().hverr())
				{err = 1; break;}
		if (err)	return std::tuple<GFSError,std::uint64_t>(GFSError(GFSErrorCode::csCpy,"fk"),0);
		else	return std::tuple<GFSError,std::uint64_t>(GFSError(GFSErrorCode::csFul,"too lage"),0);
	}
	else
	{
		for (int i = 0; i < data.size(); ++i)
			{char ch = data[i];WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
		bool err = 0;
		for (int i = 0; i < secondaries.size(); ++i)
			if (srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]),"RPCApplyMutation",handle,sern,MutationAppend,dataID,0,data.size()).get().as<GFSError>().hverr())
				{err = 1;	break;}
		if (err)	return std::tuple<GFSError,std::uint64_t>(GFSError(GFSErrorCode::csCpy,"append fail"),size);
		else	return std::tuple<GFSError,std::uint64_t>(GFSError(),size);
	}
}
GFSError ChunkServer::RPCApplyMutation(ChunkHandle handle, std::uint64_t sern, MutationType type, std::uint64_t dataID, std::uint64_t offset, std::uint64_t length)
{
	std::string Name = rootDir+std::to_string(handle), N1 = Name+"sn";
	std::fstream WC(Name.c_str(),std::ios::app|std::ios::binary);
	if (!WC)	return GFSError(GFSErrorCode::csOpen,"open file fail");
	if (dataMap.find(dataID) == dataMap.end())	return GFSError(GFSErrorCode::csRead,"no such data id");
	if (chunkMap.find(handle) == chunkMap.end())	return GFSError(GFSErrorCode::csRead,"no such chunk");
	while (chunkMap[handle].sern+1 != sern);
	++chunkMap[handle].sern;
	std::ofstream fout(N1.c_str());
	fout << sern;	fout.close();
	std::string data = dataMap[dataID];
	switch (type){
		case MutationWrite:
			WC.seekg(0,std::ios::end);while (WC.tellg() < offset){char ch = 0;WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}WC.seekp(offset);
			for (int i = 0; i < data.size(); ++i)
				{char ch = data[i];WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
			break;
		case MutationAppend:
			WC.seekg(0,std::ios::end);
			for (int i = 0; i < data.size(); ++i)
				{char ch = data[i];WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
			break;
		case MutationPad:
			WC.seekg(0,std::ios::end);
			while (WC.tellg() != CHUNK_SIZE)
				{char ch = 0;WC.write(reinterpret_cast<char*>(&ch),sizeof(char));}
			break;
	}
	WC.close();
	return GFSError();
}
GFSError ChunkServer::RPCSendCopy(ChunkHandle handle, std::string addr)
{
	std::ifstream RF((rootDir+std::to_string(handle)).c_str());
	if (!RF)	return GFSError(GFSErrorCode::csOpen,"Open file fail");
	std::string data;
	while (!RF.eof())
	{
		char ch;
		RF.read(reinterpret_cast<char*>(&ch), sizeof(char));
		data += ch;
	}	RF.close();
	if (chunkMap.find(handle) == chunkMap.end())	return GFSError(GFSErrorCode::csRead, "no such chunk");
	ChunkVersion ver = chunkMap[handle].ver;	std::uint64_t sern = chunkMap[handle].sern;
	if (!srv.RPCCall(LightDS::Service::RPCAddress::from_string(addr),"RPCApplyCopy",handle,ver,data,sern).get().as<GFSError>().hverr())	return GFSError();
	else	return GFSError(GFSErrorCode::csCpy, "");
}
GFSError ChunkServer::RPCApplyCopy(ChunkHandle handle, ChunkVersion ver, std::string data, std::uint64_t sern)
{
	std::string Name = rootDir+std::to_string(handle), N1 = Name+"sn", N2 = Name+"vs";
	std::ofstream copyFile(Name.c_str());
    if (!copyFile)	return GFSError(GFSErrorCode::csCreate, "copy fail");
	for (int i = 0; i < data.size(); ++i)
	{
		char ch = data[i];
		copyFile.write(reinterpret_cast<char*>(&ch),sizeof(char));
	}
	copyFile.close();
	std::ofstream fout1(N1.c_str()), fout2(N2.c_str());
	fout1 << sern;	fout1.close();
	fout2 << ver;	fout2.close();
	chunkMap[handle] = chunkInformation(0,ver,0,sern);
	return GFSError();
}
GFSError ChunkServer::RPCGrantLease(std::vector<std::tuple<ChunkHandle, ChunkVersion, std::uint64_t> >leaseMap)
{
	for (int i = 0; i < leaseMap.size(); ++i)
	{
		std::string Name = rootDir+std::to_string(std::get<0>(leaseMap[i])), N2 = Name+"vs";
		if (chunkMap.find(std::get<0>(leaseMap[i])) == chunkMap.end())
			return GFSError(GFSErrorCode::csOpen, "no such chunk");
		std::ofstream fout2(N2);
		fout2 << std::get<1>(leaseMap[i]);	fout2.close();
		std::uint64_t sern = chunkMap[std::get<0>(leaseMap[i])].sern;
		chunkMap[std::get<0>(leaseMap[i])] = chunkInformation(1,std::get<1>(leaseMap[i]),std::get<2>(leaseMap[i]),sern);
	}
	return GFSError();
}
GFSError ChunkServer::RPCUpdateVersion(ChunkHandle handle, ChunkVersion newVersion)
{
	std::string Name = rootDir+std::to_string(handle), N2 = Name+"vs";
	if (chunkMap.find(handle) == chunkMap.end())
		return GFSError(GFSErrorCode::csOpen, "no such chunk");
	std::ofstream fout2(N2.c_str());
	fout2 << newVersion;	fout2.close();
	
	chunkMap[handle].ver = newVersion;
	return GFSError();
}
GFSError ChunkServer::RPCPushData(std::uint64_t dataID, std::string data)
{
	if (dataMap.find(dataID) != dataMap.end())
		return GFSError(GFSErrorCode::csEx, "used data id");
	dataMap[dataID] = data;
	return GFSError();
}
