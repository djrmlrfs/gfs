#ifndef GFS_MASTER_H
#define GFS_MASTER_H

#include "comm.h"
#include "service.hpp"
class Master
{
	typedef long long ll;
	struct Cks{
		int dead;
		std::mutex lock;
		time_t last_alive;
		LightDS::Service::RPCAddress addr;
		std::map<ChunkHandle,int> chunk_map;
		Cks();
	};
	std::map<std::string,Cks*> cim;
	struct CHKmt{
		int rep_level;
		time_t expire;
		ChunkVersion ver;
		std::mutex lock;
		std::string primary;
		std::vector<std::string> rep;
		CHKmt(ChunkVersion v);
	};
	std::map<ChunkHandle,CHKmt*> cmm;
	struct RWmutex{
		std::mutex mut;
		std::atomic_int rcnt;
		RWmutex():rcnt(0){}
		void r_lock(){if (rcnt++ <= 0)	mut.lock();}
		void r_unlock(){if (rcnt > 0)	if(!(--rcnt))	mut.unlock();}
		void lock(){mut.lock();if (!rcnt)	--rcnt;}
		void unlock(){if (!(rcnt+1))	++rcnt, mut.unlock();}
	};

	struct node{
		bool is_dir;
		RWmutex mut;
		std::string name;
		std::uint64_t len;
		std::map<std::string,node*> kdz;
		std::vector<ChunkHandle> chunk_list;
		node(std::string n, bool d);
	}*root;
	GFSError ltnd(ChunkVersion &ver, std::uint64_t &exp, ChunkHandle handle);
	GFSError vset(ChunkVersion version, ChunkHandle handle);
	GFSError vget(ChunkVersion &version, ChunkHandle handle);
	struct lct{
		int r;	node *p, *t;
		std::string last;std::vector<node*> lks;
		lct(std::string &path, int rd, node *rt);
		~lct();
	};
	template <typename Ret, typename ...Args>
	void bind(std::string rpcName, Ret(Master::*func)(Args...));
	GFSError rfind(std::set<std::string> &rep, int num);
	GFSError radd(std::string address, ChunkHandle handle);
	void gbgfind(std::vector<ChunkHandle> &gbg, node *t);
	GFSError rrmv(ChunkHandle handle, std::string address);
	GFSError crmv(ChunkHandle handle);
public:
	Master(LightDS::Service &srv, const std::string &rootDir);
	void clr(node *t);
	~Master();
	void Start();
	void Shutdown();
public:
	void BackgroundActivity();
	std::tuple<GFSError,std::vector<ChunkHandle> > RPCHeartbeat(std::vector<ChunkHandle> leaseExtensions, std::vector<std::tuple<ChunkHandle, ChunkVersion>> chunks, std::vector<ChunkHandle> failedChunks);
	std::tuple<GFSError,std::string,std::vector<std::string>,std::uint64_t>	RPCGetPrimaryAndSecondaries(ChunkHandle handle);
	std::tuple<GFSError,std::vector<std::string> > RPCGetReplicas(ChunkHandle handle);
	std::tuple<GFSError,bool,std::uint64_t,std::uint64_t> RPCGetFileInfo(std::string path);
	GFSError RPCMkdir(std::string path);
	GFSError RPCCreateFile(std::string path);
	GFSError RPCDeleteFile(std::string path);
	std::tuple<GFSError,std::vector<std::string> > RPCListFile(std::string path);
	std::tuple<GFSError, ChunkHandle> RPCGetChunkHandle(std::string path, std::uint64_t chunkIndex);
protected:
	int on;
	LightDS::Service &srv;
	std::string rootDir;
};
#endif
