#ifndef GFS_CHUNKSERVER_H
#define GFS_CHUNKSERVER_H

#include "comm.h"
#include <dirent.h>
#include <string>
#include <fstream>
#include "service.hpp"
class ChunkServer
{
	template<typename Ret, typename ...Args>
	void bind(std::string rpcName, Ret(ChunkServer::*func)(Args...));
public:
	struct chunkInformation{
		bool isp;
		ChunkVersion ver;
		std::uint64_t sern, tsmp;
		chunkInformation();
		chunkInformation(const bool&ip, const ChunkVersion&vs, const std::uint64_t&ts, const std::uint64_t&sn);
	};
	enum MutationType : std::uint32_t
	{
		MutationWrite,
		MutationAppend,
		MutationPad
	};
	ChunkServer(LightDS::Service &srv, const std::string &rootDir);
	void Start();
	void Shutdown();
public:
	void Heartbeat();
	GFSError RPCCreateChunk(ChunkHandle handle);
	std::tuple<GFSError, std::string> RPCReadChunk(ChunkHandle handle, std::uint64_t offset, std::uint64_t length);
	GFSError RPCWriteChunk(ChunkHandle handle, std::uint64_t dataID, std::uint64_t offset, std::vector<std::string> secondaries);
	std::tuple<GFSError, std::uint64_t> RPCAppendChunk(ChunkHandle handle, std::uint64_t dataID, std::vector<std::string> secondaries);
	GFSError RPCApplyMutation(ChunkHandle handle, std::uint64_t sern, MutationType type, std::uint64_t dataID, std::uint64_t offset, std::uint64_t length);
	GFSError RPCSendCopy(ChunkHandle handle, std::string addr);
	GFSError RPCApplyCopy(ChunkHandle handle, ChunkVersion ver, std::string data, std::uint64_t sern);
	GFSError RPCGrantLease(std::vector<std::tuple<ChunkHandle, ChunkVersion, std::uint64_t> >leaseMap);
	GFSError RPCUpdateVersion(ChunkHandle handle, ChunkVersion newVersion);
	GFSError RPCPushData(std::uint64_t dataID, std::string data);
protected:
	LightDS::Service &srv;
	std::string rootDir;
private:
	bool on;
	std::map<std::uint64_t, std::string> dataMap;
    std::map<ChunkHandle, chunkInformation> chunkMap;
};
MSGPACK_ADD_ENUM(ChunkServer::MutationType);
#endif
