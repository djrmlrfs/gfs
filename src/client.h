#ifndef GFS_CLIENT_H
#define GFS_CLIENT_H

#include "comm.h"
#include "user.hpp"

class Client
{
public:
	Client(LightDS::User &srv);
	GFSError Create(const std::string &path);
	GFSError Mkdir(const std::string &path);
	std::tuple<GFSError, std::vector<std::string> >List(const std::string &path);
	std::tuple<GFSError, size_t>Read(const std::string &path, std::uint64_t offset, std::vector<char> &data);
	GFSError Write(const std::string &path, std::uint64_t offset, const std::vector<char> &data);
	std::tuple<GFSError, std::uint64_t>Append(const std::string &path, const std::vector<char> &data);
	std::tuple<GFSError, ChunkHandle> GetChunkHandle(const std::string &path, std::uint64_t index);
	std::tuple<GFSError, size_t> ReadChunk(ChunkHandle handle, std::uint64_t offset, std::vector<char> &data);
	GFSError WriteChunk(ChunkHandle handle, std::uint64_t offset, const std::vector<char> &data);
	std::tuple<GFSError, std::uint64_t>	AppendChunk(ChunkHandle handle, const std::vector<char> &data);
protected:
	LightDS::User &srv;
};
#endif
