#include "client.h"
Client::Client(LightDS::User &srv):srv(srv){srv.Run();}
GFSError Client::Create(const std::string &path)
{
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	return srv.RPCCall(addr,"RPCCreateFile",path).get().as<GFSError>();
}
GFSError Client::Mkdir(const std::string &path)
{
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	return srv.RPCCall(addr,"RPCMkdir",path).get().as<GFSError>();
}
std::tuple<GFSError, std::vector<std::string> > Client::List(const std::string &path)
{
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	return srv.RPCCall(addr,"RPCListFile",path).get().as<std::tuple<GFSError, std::vector<std::string> >>();
}
std::tuple<GFSError, size_t> Client::Read(const std::string &path, std::uint64_t offset, std::vector<char> &data)
{
	int lft = data.size(), occ = offset/CHUNK_SIZE;
	auto ckh = GetChunkHandle(path,occ);
	if (std::get<0>(ckh).hverr())	return std::tuple<GFSError,size_t>(std::get<0>(ckh),0);
	std::uint64_t omc = offset%CHUNK_SIZE;
	size_t sz = 0;
	while (lft > 0)
	{
		std::vector<char> z;
		if (lft+omc > CHUNK_SIZE)	z.resize(CHUNK_SIZE-omc);
		else	z.resize(lft);
		lft -= CHUNK_SIZE-omc;
		auto tmp = ReadChunk(std::get<1>(ckh),omc,z);
		if (std::get<0>(tmp).hverr())	return std::tuple<GFSError,size_t>(std::get<0>(tmp),0);
		if (lft <= 0)	break;		data.resize(z.size());
		ckh = GetChunkHandle(path,++occ);
		for (int i = 0; i < z.size(); ++i)	data[sz++] = z[i];
		if (std::get<0>(ckh).hverr())	return std::tuple<GFSError,size_t>(std::get<0>(ckh),0);
		omc = 0;
	}
	return std::tuple<GFSError,size_t>(GFSError(),sz);
}
GFSError Client::Write(const std::string &path, std::uint64_t offset, const std::vector<char> &data)
{
	int lft = data.size(), occ = offset/CHUNK_SIZE;
	auto ckh = GetChunkHandle(path,occ);
	if (std::get<0>(ckh).hverr())	return std::get<0>(ckh);
	std::uint64_t omc = offset%CHUNK_SIZE;
	size_t sz = 0;
	while (lft > 0)
	{
		std::vector<char> z;
		if (lft+omc > CHUNK_SIZE)	z.resize(CHUNK_SIZE-omc);
		else	z.resize(lft);
		for (int i = (int)z.size()-1; i >= 0; --i)	z[i] = data[i+sz];
		lft -= CHUNK_SIZE-omc;
		GFSError err = WriteChunk(std::get<1>(ckh),omc,z);
		if (err.hverr())	return err;
		sz += z.size();	lft -= (CHUNK_SIZE-omc);
		if (lft <= 0)	break;	std::tuple<GFSError,ChunkHandle> t = GetChunkHandle(path,++occ);
		if (std::get<0>(ckh).hverr())	return std::get<0>(ckh);
        omc = 0;
	}
	return GFSError();
}
std::tuple<GFSError, std::uint64_t> Client::Append(const std::string &path, const std::vector<char> &data)
{
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	std::tuple<GFSError,bool,std::uint64_t,std::uint64_t> tmp = srv.RPCCall(addr,"RPCGetFileInfo",path).get().as<std::tuple<GFSError,bool,std::uint64_t,std::uint64_t> >();
	if (std::get<0>(tmp).hverr())	return std::tuple<GFSError,std::uint64_t>(std::get<0>(tmp),0);
	std::uint64_t chunks = std::get<3>(tmp)-1;	if (chunks < 0)	chunks = 0;
	std::tuple<GFSError,ChunkHandle> t = GetChunkHandle(path,chunks-1);
	if (std::get<0>(t).hverr())	return std::tuple<GFSError,std::uint64_t>(std::get<0>(t),0);
	GFSError err;	std::uint64_t off;
	std::tie(err,off) = AppendChunk(std::get<1>(t),data);
	if (err.errCode == GFSErrorCode::csFul)
	{
		t = GetChunkHandle(path,++chunks);
		if (std::get<0>(t).hverr())	return std::tuple<GFSError,std::uint64_t>(std::get<0>(t),0);
		std::tie(err,off) = AppendChunk(std::get<1>(t),data);
	}
	off += chunks*CHUNK_SIZE;
	return std::make_tuple(err,off);
}
std::tuple<GFSError, ChunkHandle> Client::GetChunkHandle(const std::string &path, std::uint64_t index)
{
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	return srv.RPCCall(addr,"RPCGetChunkHandle",path,index).get().as<std::tuple<GFSError,ChunkHandle>>();	
}
std::tuple<GFSError, size_t> Client::ReadChunk(ChunkHandle handle, std::uint64_t offset, std::vector<char> &data)
{
	LightDS::User::RPCAddress addr = srv.ListService("chunkserver")[0];
	auto tmp = srv.RPCCall(addr,"RPCReadChunk",handle,offset,data.size()).get().as<std::tuple<GFSError, std::string> >();
	GFSError err = std::get<0>(tmp);
	std::string &s = std::get<1>(tmp);
	data.assign(s.begin(),s.end());
	return std::tuple<GFSError,size_t>(err,s.size());
}
GFSError Client::WriteChunk(ChunkHandle handle, std::uint64_t offset, const std::vector<char> &data)
{
	std::string s;
	for (int i = 0; i < data.size(); ++i)	s += data[i];
	std::uint64_t dataId = (((uint64_t)time(0)<<32)|(int)(handle));
	
	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	auto tmp = srv.RPCCall(addr,"RPCGetReplicas",handle).get().as<std::tuple<GFSError,std::vector<std::string>>>();
	int _ = std::get<1>(tmp).size();
	for (int i = 0; i < _; ++i)
	{
		GFSError er = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(tmp)[i]),"RPCPushData",dataId,s).get().as<GFSError>();
		if (er.hverr())	return er;
	}
	auto t2 = srv.RPCCall(addr,"RPCGetPrimaryAndSecondaries",handle).get().as<std::tuple<GFSError,std::string,std::vector<std::string>,std::uint64_t> >();
	if (std::get<0>(t2).hverr())	return std::get<0>(t2);
	return srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(t2)),"RPCWriteChunk",handle,dataId,offset,std::get<2>(t2)).get().as<GFSError>();
}
std::tuple<GFSError, std::uint64_t> Client::AppendChunk(ChunkHandle handle, const std::vector<char> &data)
{
	std::string s;
	for (int i = 0; i < data.size(); ++i)	s += data[i];
	std::uint64_t dataId = (((uint64_t)time(0)<<32)|(int)(handle));

	LightDS::User::RPCAddress addr = srv.ListService("master")[0];
	auto tmp = srv.RPCCall(addr,"RPCGetReplicas",handle).get().as<std::tuple<GFSError,std::vector<std::string>> >();
	for (int i = 0; i < std::get<1>(tmp).size(); ++i)
		GFSError er = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(tmp)[i]),"RPCPushData",dataId,s).get().as<GFSError>();
	auto t2 = srv.RPCCall(addr,"RPCGetPrimaryAndSecondaries",handle).get().as<std::tuple<GFSError,std::string,std::vector<std::string>,std::uint64_t> >();
	if (std::get<0>(t2).hverr())	return std::tuple<GFSError,std::uint64_t>(std::get<0>(t2),0);
	return srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(t2)),"RPCAppendChunk",handle,dataId,std::get<2>(t2)).get().as<std::tuple<GFSError, std::uint64_t> >();
}
