#include "master.h"
Master::node::node(std::string n, bool d):name(n),is_dir(d),len(0){}
Master::lct::lct(std::string &path, int rd, node *rt): p(rt), t(NULL), r(-1)
{
	int i = 1, l = path.length();
	while (i < l)
	{
		last.clear();
		while (i<l && path[i]!='/')	last += path[i++];
		if (i<l && path[i]=='/')	++i;
		p->mut.r_lock();	lks.push_back(p);
		t = p->kdz[last];	if (i >= l)	break;	if (!(p=t)) return;
	}
	if (last.back() == '/')	last.pop_back();	if (!t)	return;
	if (rd)	t->mut.r_lock();	else	t->mut.lock();
	r = rd, lks.push_back(t);
}
Master::lct::~lct()
{
	if (r == 1)	lks[lks.size()-1]->mut.r_unlock(), lks.pop_back();
	else if (r == 0)	lks[lks.size()-1]->mut.unlock(), lks.pop_back();
	for (int i = lks.size()-1; i >= 0; --i)	lks[i]->mut.r_unlock();
}
Master::Cks::Cks(): dead(0), last_alive(time(0)) {}
Master::CHKmt::CHKmt(ChunkVersion v): ver(v), rep_level(3), expire(0) {}
template <typename Ret, typename ...Args>
void Master::bind(std::string rpcName, Ret(Master::*func)(Args...)){srv.RPCBind(rpcName, std::function<Ret(Args...)>([this, func](Args ...args)->Ret {return (this->*func)(std::forward<Args>(args)...);}));}
Master::Master(LightDS::Service &srv, const std::string &rootDir): srv(srv), rootDir(rootDir), on(0)
{
	srand(time(0));
	root = new node("/",1);
	bind("RPCMkdir", &Master::RPCMkdir);
	bind("RPCListFile", &Master::RPCListFile);
	bind("RPCHeartbeat", &Master::RPCHeartbeat);
	bind("RPCCreateFile", &Master::RPCCreateFile);
	bind("RPCDeleteFile", &Master::RPCDeleteFile);
	bind("RPCGetReplicas", &Master::RPCGetReplicas);
	bind("RPCGetFileInfo", &Master::RPCGetFileInfo);
	bind("RPCGetChunkHandle", &Master::RPCGetChunkHandle);
	bind("RPCGetPrimaryAndSecondaries", &Master::RPCGetPrimaryAndSecondaries);
}
void Master::clr(node *t){for (auto it = t->kdz.begin(); it != t->kdz.end(); ++it)if (it->second)	clr(it->second);	delete t;}
Master::~Master(){clr(root);}
void Master::Start(){on = 1;/*...*/}
void Master::Shutdown(){on = 0;}
GFSError Master::rfind(std::set<std::string> &rep, int num)
{
	std::vector<std::string> ret;
	for (auto it = cim.begin(); it != cim.end(); ++it){
		if (!it->second)	continue;
		if (!it->second->dead && rep.find(it->first) != rep.end())	ret.push_back(it->first);
	}
	if (ret.size() < num)	return GFSError(GFSErrorCode::mLksev,std::to_string(num));
	rep.clear();
	for (int i = 0; i < num; ++i)	rep.insert(ret[i]);
	return GFSError();
}
GFSError Master::radd(std::string address, ChunkHandle handle)
{
	Cks *t = cim[address];
	if (!t) return GFSError(GFSErrorCode::mNosev,address);
	std::lock_guard<std::mutex>(t->lock);
	t->chunk_map[handle] = 1;	return GFSError();
}

void Master::gbgfind(std::vector<ChunkHandle> &gbg, node *t)
{
	for (auto it = t->kdz.begin(); it != t->kdz.end(); ++it)
	if (it->second->is_dir)	gbgfind(gbg, it->second);
	else if (it->second->name.substr(0, 1) == "$")
	{
		int p = 1;
		while (it->second->name[p] != '_')	++p;
		time_t t = std::stoi(it->second->name.substr(1,p-1));
		if (GPas < std::chrono::seconds(time(0)-t))
			gbg.insert(gbg.begin(),it->second->chunk_list.begin(),it->second->chunk_list.end());
	}
}
GFSError Master::rrmv(ChunkHandle handle, std::string address)
{
	CHKmt* p = cmm[handle];
	if (!p)	return GFSError(GFSErrorCode::mNock,std::to_string(handle));
	std::lock_guard<std::mutex>(p->lock);
	for (auto it = p->rep.begin(); it != p->rep.end(); ++it)
		if (*it == address)	{p->rep.erase(it);	return GFSError();}
	return GFSError(GFSErrorCode::mNorp,address);
}
GFSError Master::crmv(ChunkHandle handle)
{
	CHKmt* p = cmm[handle];
	if (!p) return GFSError(GFSErrorCode::mNock,std::to_string(handle));
	delete p;	cmm.erase(handle);	return GFSError();
}
void Master::BackgroundActivity()
{
	while (on)
	{
		for (auto it = cim.begin(); it != cim.end(); ++it)	
		{
			time_t now = time(0);
			if (!it->second)	continue;
			if (std::chrono::seconds(now-it->second->last_alive) > St)
			{
				it->second->dead = 1;
				for (auto cit = it->second->chunk_map.begin(); cit != it->second->chunk_map.end(); ++cit)
					if (cit->second != 0)	rrmv(cit->first, it->first);
				Cks *t = cim[it->first];
				std::lock_guard<std::mutex>(t->lock);
				delete t;	cim.erase(it->first);
			}
		}
		std::vector<ChunkHandle> gbg;
		gbgfind(gbg,root);
		for (auto it = gbg.begin(); it != gbg.end(); ++it) crmv(*it);
		for (auto it = cmm.begin(); it != cmm.end(); ++it)
		{
			int t = it->second->rep_level-it->second->rep.size();
			if (t > 0)
			{
				std::set<std::string>rep;
				for (auto i : it->second->rep)	rep.insert(i);
				GFSError err = rfind(rep,t);
				if (err.hverr())	continue;
				for (int j = 0; j<it->second->rep.size() && rep.size(); ++j)	for (auto i : rep)
				{
					LightDS::Service::RPCAddress rpc_addr = cim[it->second->rep[j]]->addr;
					err = srv.RPCCall(rpc_addr,"RPCSendCopy",it->first,cim[i]->addr.to_string()).get().as<GFSError>();
					if (!err.hverr())	radd(i,it->first), rep.erase(i);
					else	break;
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::seconds(BPas));
	}
}
GFSError Master::ltnd(ChunkVersion &ver, std::uint64_t &exp, ChunkHandle handle)
{
	CHKmt* p = cmm[handle];
	if (!p) return GFSError(GFSErrorCode::mNock,std::to_string(handle));
	std::lock_guard<std::mutex>(p->lock);
	exp = p->expire = time(0), ver = (++p->ver);
	return GFSError();
}
GFSError Master::vset(ChunkVersion version, ChunkHandle handle)
{
	CHKmt* p = cmm[handle];
	if (!p) return GFSError(GFSErrorCode::mNock,std::to_string(handle));
	std::lock_guard<std::mutex>(p->lock);
	p->ver = version;	return GFSError();
}
GFSError Master::vget(ChunkVersion &version, ChunkHandle handle)
{
	CHKmt* p = cmm[handle];
	if (!p)	return GFSError(GFSErrorCode::mNock,std::to_string(handle));
	version = p->ver;	return GFSError();
}
std::tuple<GFSError,std::vector<ChunkHandle> > Master::RPCHeartbeat(std::vector<ChunkHandle> leaseExtensions, std::vector<std::tuple<ChunkHandle, ChunkVersion>> chunks, std::vector<ChunkHandle> failedChunks)
{
	GFSError err;
	std::vector<ChunkHandle> gbg;
	std::string address = srv.getRPCCaller();
	Cks *t = cim[address];
	if (!t)	{if (!cim[address])
	{
		std::vector<LightDS::Service::RPCAddress> vec = srv.ListService("chunkserver");
		for (auto i : vec)	if (i.ip == address)
		{
			Cks *t = new Cks();
			cim[address] = t, t->addr = i;
		}
	}}
	else	{std::lock_guard<std::mutex>(t->lock);t->last_alive = time(0), t->dead = 0;}
	for (auto it = chunks.begin(); it != chunks.end(); ++it)
	{
		ChunkHandle handle = std::get<0>(*it);
		ChunkVersion ver = 0, cver = std::get<1>(*it);
		err = vget(ver,handle);
		if (err.hverr())	{gbg.push_back(handle);continue;}
		if (ver > cver)	{gbg.push_back(handle);rrmv(handle, address);continue;}
		else if (ver < cver)	err = vset(cver,handle);
		CHKmt* p = cmm[handle];
		if (!p)	continue;	int ok = 1;
		std::lock_guard<std::mutex>(p->lock);
		for (auto it = p->rep.begin(); it != p->rep.end(); ++it)
			if (*it == address)	{ok = 0;	break;}
		if (ok)	p->rep.push_back(address);
	}
	for (auto it = failedChunks.begin(); it != failedChunks.end(); ++it)	err = rrmv(*it,address);
	std::vector<std::tuple<ChunkHandle,ChunkVersion,std::uint64_t>> vec;
	for (auto it = leaseExtensions.begin(); it != leaseExtensions.end(); ++it)
	{
		ChunkVersion ver = 0;
		std::uint64_t exp = 0;
		err = ltnd(ver,exp,*it);
		if (!err.hverr())	vec.push_back(std::tuple<ChunkHandle,ChunkVersion,std::uint64_t>(*it,ver,exp));
	}
	
	LightDS::Service::RPCAddress rpc_addr = cim[address]->addr;
	err = srv.RPCCall(rpc_addr,"RPCGrantLease",vec).get().as<GFSError>();
	return std::tuple <GFSError,std::vector<ChunkHandle>>(err,gbg);
}
std::tuple<GFSError,std::string,std::vector<std::string>,std::uint64_t>Master::RPCGetPrimaryAndSecondaries(ChunkHandle handle)
{
	GFSError err;
	std::string prim;
	std::uint64_t exp;
	std::vector<std::string> sec;
	CHKmt* p = cmm[handle];
	if (!p)	err = GFSError(GFSErrorCode::mNock,std::to_string(handle));else{
	std::lock_guard<std::mutex>(p->lock);sec = p->rep;
	if (std::chrono::seconds(time(0)-p->expire) > LMax)
	{
		++p->ver;
		for (auto i : sec)
		{
			LightDS::Service::RPCAddress rpc_addr = cim[i]->addr;
			err = srv.RPCCall(rpc_addr,"RPCUpdateVersion",handle,p->ver).get().as<GFSError>();
		}
		p->primary = p->rep[rand()%(p->rep.size())], p->expire = time(0);
		std::vector<std::tuple<ChunkHandle,ChunkVersion,std::uint64_t>> lease;
		lease.push_back(std::tuple<ChunkHandle,ChunkVersion,std::uint64_t>(handle,p->ver,p->expire));
		
		LightDS::Service::RPCAddress rpc_addr = cim[p->primary]->addr;
		err = srv.RPCCall(rpc_addr,"RPCGrantLease",lease).get().as<GFSError>();
	}
	prim = p->primary, exp = p->expire;
	for (auto it = sec.begin(); it != sec.end(); ++it)	if (*it == prim)
		{sec.erase(it);	break;}
	}
	prim = cim[prim]->addr.to_string();
	for (auto & i : sec)	i = cim[i]->addr.to_string();
	return std::tuple<GFSError,std::string,std::vector<std::string>,std::uint64_t>(err,prim,sec,exp);
}
std::tuple<GFSError,std::vector<std::string> > Master::RPCGetReplicas(ChunkHandle handle)
{
	GFSError err;
	std::vector<std::string> vec;
	CHKmt* p = cmm[handle];
	if (!p)	err = GFSError(GFSErrorCode::mNock,std::to_string(handle));
	else	vec = p->rep;
	for (auto & i : vec)	i = cim[i]->addr.to_string();
	return std::tuple <GFSError, std::vector<std::string>>(err, vec);
}
std::tuple<GFSError,bool,std::uint64_t,std::uint64_t> Master::RPCGetFileInfo(std::string path)
{
	bool is_dir;	GFSError err;
	std::uint64_t len = 0, chunk = 0;
	lct lck(path,1,root);
	node *p = lck.p, *t = lck.t;
	if (!p || !t)	err = GFSError(GFSErrorCode::mNofl,path);
	else
	{
		is_dir = t->is_dir;
		len = is_dir?0:t->len;
		chunk = is_dir?0:t->chunk_list.size();
	}
	return std::tuple<GFSError,bool,std::uint64_t,std::uint64_t>(err,is_dir,len,chunk);
}
GFSError Master::RPCMkdir(std::string path)
{
	lct lck(path, 0, root);
	std::string last = lck.last;
	node *p = lck.p, *t = lck.t;
	if (!p) return GFSError(GFSErrorCode::mNofl,path);
	if (t) return GFSError(GFSErrorCode::mHvfl,path);
	if (last.empty()) return GFSError(GFSErrorCode::mWrnm,path);
	node *n = new node(last,1);
	p->kdz[last] = n;
	return GFSError();
}
GFSError Master::RPCCreateFile(std::string path)
{
	lct lck(path, 0, root);
	std::string last = lck.last;
	node *p = lck.p, *t = lck.t;
	if (!p) return GFSError(GFSErrorCode::mNofl,path);
	if (t) return GFSError(GFSErrorCode::mHvfl,path);
	if (last.empty()) return GFSError(GFSErrorCode::mWrnm,path);
	node *n = new node(last,0);
	p->kdz[last] = n;
	return GFSError();
}
GFSError Master::RPCDeleteFile(std::string path)
{
	lct lck(path,0,root);
	std::string last = lck.last;
	node *p = lck.p, *t = lck.t;
	if (!p || !t) return GFSError(GFSErrorCode::mNofl,path);
	if(t->is_dir)
	{
		if (!t->kdz.empty()) return GFSError(GFSErrorCode::mDr,path);
		delete t;
		p->kdz.erase(last);
	}
	else
	{
		std::string prefix = "$"+std::to_string(time(0))+'_';
		t->name.insert(0, prefix);
		p->kdz.erase(last);
		p->kdz[t->name] = t;
	}
	return GFSError();
}
std::tuple<GFSError,std::vector<std::string> > Master::RPCListFile(std::string path)
{
	GFSError err;
	std::vector<std::string> vec;
	lct lck(path, 1, root);
	node *p = lck.p, *t = lck.t;
	if (!p || !t)	err = GFSError(GFSErrorCode::mNofl,path);
	else if (!t->is_dir) err = GFSError(GFSErrorCode::mFl,path);
	else for (auto it = t->kdz.begin(); it != t->kdz.end(); ++it)
	{
		it->second->mut.r_lock();
		vec.push_back(it->second->name);
		it->second->mut.r_unlock();
	}
	return std::tuple <GFSError, std::vector<std::string>>(err, vec);
}
std::tuple<GFSError,ChunkHandle> Master::RPCGetChunkHandle(std::string path, std::uint64_t chunkIndex)
{
	GFSError err;
	lct lck(path,1,root);
	node *p = lck.p, *t = lck.t;
	if (!p || !t)	err = GFSError(GFSErrorCode::mNofl,path);
	else if (t->is_dir)	err = GFSError(GFSErrorCode::mNFl,path);
	else if (t->chunk_list.size() < chunkIndex)
		err = GFSError(GFSErrorCode::mWrid,path+'['+std::to_string(chunkIndex)+']');
	else if (t->chunk_list.size() == chunkIndex)
	{
		ChunkHandle nh;
		do {nh = ((ll)time(0)<<32)|(int)(rand()*rand());}while (cmm[nh]);
		std::vector<std::string> rep;
		CHKmt *nm = new CHKmt(1);
		int num = nm->rep_level;
		for (auto it = cim.begin(); it != cim.end(); ++it)
			if (!it->second)	continue;
			else if (!it->second->dead)	rep.push_back(it->first);
		for (int i = 0; i < rep.size()-num; ++i)	rep.pop_back();
		cmm[nh] = nm;
		for (auto i : rep)
		{
			LightDS::Service::RPCAddress rpc_addr = cim[i]->addr;
			err = srv.RPCCall(rpc_addr,"RPCCreateChunk",nh).get().as<GFSError>();
			if (!err.hverr())	nm->rep.push_back(i);
		}
		t->chunk_list.push_back(nh);
	}
	ChunkHandle handle = t->chunk_list[chunkIndex];
	return std::tuple <GFSError,ChunkHandle>(err,handle);
}
